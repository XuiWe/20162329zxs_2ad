package exp1;

import junit.framework.TestCase;

import java.util.LinkedList;

/**
 * Created by 蜡笔小新丶 on 2017/9/25.
 */
public class LinkedListTTest extends TestCase {
    LinkedList<Integer> list = new LinkedList();
    public LinkedListTTest(){
        int A = 1;
        for(int i=0;i<4;i++){
            list.add(A);
            A = A * 10;
        }
    }

    public void testRemoveFirst() throws Exception {
        assertEquals("1",list.removeFirst()+"");
    }

    public void testRemoveLast() throws Exception {
        assertEquals("1000",list.removeLast()+"");
    }

    public void testContains() throws Exception {
        assertEquals(false,list.contains(10000));
        assertEquals(true,list.contains(100));
    }

    public void testAdd() throws Exception {
        assertEquals(true,list.add(1000000));
    }

    public void testRemove() throws Exception {
        int result;
        result = list.remove(3);
        assertEquals(1000,result);
    }

}