package exp1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 蜡笔小新丶 on 2017/9/25.
 */
public class merge {
    ArrayList list;
    public List<? extends Comparable> mergeSortedList(List<? extends Comparable> aList,
                                                      List<? extends Comparable> bList){
        list = new ArrayList();
        for(Comparable i:aList)
            list.add(i);
        for (Comparable i:bList)
            list.add(i);
        Sort(list);
        return list;
    }

    public void Sort(List<? extends Comparable> list){
        int min;

        for (int index = 0; index < list.size()-1; index++)
        {
            min = index;
            for (int scan = index+1; scan < list.size(); scan++)
                if (list.get(scan).compareTo(list.get(min)) < 0)
                    min = scan;
            swap((List<Comparable>) list, min, index);
        }
    }
    private static void swap ( List<Comparable> list, int index1, int index2)
    {
        Comparable temp = list.get(index1);
        Comparable A =  list.get(index2);
        list.set(index1,A);
        list.set(index2,temp);
    }

    @Override
    public String toString() {
        return "merge{" +
                "list=" + list +
                '}';
    }
}
