package exp1;

/**
 * Created by 蜡笔小新丶 on 2017/9/26.
 */
public class MyLinkedList<T> {
    Node head = null;
    public Node<T> sethead(Node<T> node){
        head = node;
        return head;
    }
    public Node<T> add(T value){
        Node<T> node = head;
        if(head==null)
            head = new Node<T>(value);
        else{
            while (true) {
                node = node.getNext();
                if(node==null) {
                    node = new Node<>(value);
                    break;
                }
            }
        }
        return node;
    }

    public T get(int Index){
        T Value = null;
        Node<T> node = head;
        if(Index==0) {
            Value = node.getValue();
        }
        if(node.getNext()!=null){
        for (int i = 0; i < Index; i++) {
                node = node.getNext();
                Value = node.getValue();
            }
        }
        return Value;
    }

    public boolean remove(T value){
        Node<T> node = head;
        T Value;
        boolean result = false;
        do {
            if(node.getValue().equals (value)) {
                node.setValue(null);
                result = true;
            }
        }while ((node = node.getNext())!=null);
        return result;
    }

    @Override
    public String toString() {
        return "MyLinkedList{" +
                "head=" + head +
                '}';
    }
}
