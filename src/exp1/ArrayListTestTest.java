package exp1;

import junit.framework.TestCase;

import java.util.ArrayList;

/**
 * Created by 蜡笔小新丶 on 2017/9/25.
 */
public class ArrayListTestTest extends TestCase {
    ArrayList<String> list = new ArrayList<>();

    public void testIsEmpty() throws Exception {
        boolean result = false;
        result = list.isEmpty();
        assertEquals(true,result);
        list.add("Zhangxusheng");
        result = list.isEmpty();
        assertEquals(true,result);
    }

    public void testAdd() throws Exception {
        boolean result = false;
        result = list.add("Caiwencheng");
        assertEquals(true,result);
    }

    public void testRemove() throws Exception {
        boolean result;
        list.add("Zhangxusheng");
        list.add("Caiwencheng");
        result = list.remove("Zhangxusheng");
        assertEquals(true,result);
        result = list.remove("Qilifeng");
        assertEquals(true,result);

    }

}