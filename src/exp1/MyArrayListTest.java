package exp1;

/**
 * Created by 蜡笔小新丶 on 2017/9/25.
 */
public class MyArrayListTest {
    public static void main(String[] args) {
        MyArrayList<String> list = new MyArrayList<>();

        System.out.println("初始list：");
        System.out.println(list);
        System.out.println("初始list为空：");
        System.out.println(list.isEmptey());
        System.out.println("添加一个元素打印list内容：");
        list.add("Zxs");
        System.out.println(list);
        System.out.println("list是否为空：");
        System.out.println(list.isEmptey());

        System.out.println("添加再添加九个元素打印list内容：");
        for (int i=0;i<10;i++){
            list.add("Cwc");
        }
        System.out.println(list);
        System.out.println("再添加一个元素打印list内容：");
        list.add("Zty");
        System.out.println(list);

        System.out.println("删除Cwc元素打印list：");
        list.remove("Cwc");
        System.out.println(list);
    }
}
