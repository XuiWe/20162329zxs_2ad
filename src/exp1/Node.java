package exp1;

/**
 * Created by 蜡笔小新丶 on 2017/9/26.
 */
public class Node<T> {
    private Node<T> Next;
    private T Value;

    public Node(){
        Next = null;
        Value = null;
    }
    public Node(T value){
        Next = null;
        Value = value;
    }

    public Node<T> getNext(){
        return Next;
    }

    public void setNext(Node<T> node){
        Next = node;
    }

    public T getValue(){
        return Value;
    }
    public void setValue(T value){
        Value = value;
    }
}
