package test.ActivityPractice; 

import ActivityPractice.Accumulation;
import junit.framework.Test;
import junit.framework.TestSuite; 
import junit.framework.TestCase; 

/** 
* Accumulation Tester. 
* 
* @author <Authors name> 
* @since <pre>09/23/2017</pre> 
* @version 1.0 
*/ 
public class AccumulationTest extends TestCase { 
public AccumulationTest(String name) { 
super(name); 
} 

public void setUp() throws Exception { 
super.setUp(); 
} 

public void tearDown() throws Exception { 
super.tearDown(); 
} 

/** 
* 
* Method: sum(int arr[][]) 
* 
*/ 
public void testSum() throws Exception { 
//TODO: Test goes here...
    Accumulation a = new Accumulation();
    int arr[][] = {{1, 2, 3, 4, 5, 6, 7, 8, 9},
            {1, 2, 3, 4, 5, 6, 7, 8, 9},
            {1, 2, 3, 4, 5, 6, 7, 8, 9},
            {1, 2, 3, 4, 5, 6, 7, 8, 9}};
    int arr1[][] = new int[10][10];
    assertEquals(a.sum(arr1),0);
    assertEquals(a.sum(arr),180);
} 



public static Test suite() { 
return new TestSuite(AccumulationTest.class); 
} 
} 
