package test.ActivityPractice; 

import ActivityPractice.SortSmall;
import junit.framework.Test;
import junit.framework.TestSuite; 
import junit.framework.TestCase; 

/** 
* SortSmall Tester. 
* 
* @author <Authors name> 
* @since <pre>09/23/2017</pre> 
* @version 1.0 
*/ 
public class SortSmallTest extends TestCase { 
public SortSmallTest(String name) { 
super(name); 
} 

public void setUp() throws Exception { 
super.setUp(); 
} 

public void tearDown() throws Exception { 
super.tearDown(); 
} 

/** 
* 
* Method: Sort(int a, int b, int c) 
* 
*/ 
public void testSort() throws Exception { 
//TODO: Test goes here...
    SortSmall small = new SortSmall();
    assertEquals(small.Sort(7,6,9),"6 7 9 ");
} 


/** 
* 
* Method: swap(int [] data, int index1, int index2) 
* 
*/ 
public void testSwap() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = SortSmall.getClass().getMethod("swap", int.class, int.class, int.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 


public static Test suite() { 
return new TestSuite(SortSmallTest.class); 
} 
} 
