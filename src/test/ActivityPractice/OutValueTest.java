package test.ActivityPractice; 

import ActivityPractice.OutValue;
import junit.framework.Test;
import junit.framework.TestSuite; 
import junit.framework.TestCase; 

/** 
* OutValue Tester. 
* 
* @author <Authors name> 
* @since <pre>09/24/2017</pre> 
* @version 1.0 
*/ 
public class OutValueTest extends TestCase { 
public OutValueTest(String name) { 
super(name); 
} 

public void setUp() throws Exception { 
super.setUp(); 
} 

public void tearDown() throws Exception { 
super.tearDown(); 
} 

/** 
* 
* Method: add(int num) 
* 
*/ 
public void testAdd() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: Out() 
* 
*/ 
public void testOut() throws Exception { 
//TODO: Test goes here...
    OutValue out = new OutValue();
    out.add(100); //输入的是从1开始顺序的一百个奇数
    assertEquals(out.Out(),"最大值：199； 最小值：1");
} 



public static Test suite() { 
return new TestSuite(OutValueTest.class); 
} 
} 
