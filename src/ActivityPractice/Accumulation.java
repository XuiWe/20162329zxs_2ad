package ActivityPractice;

import java.util.AbstractMap;

/**
 * Created by 蜡笔小新丶 on 2017/9/23.
 */
public class Accumulation {

    public int sum(int arr[][]){
        int result = 0;
        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr[i].length;j++){
                result += arr[i][j];
            }
        }
        return result;
    }
}
