package ActivityPractice;

import chapter13.Sorting;

/**
 * Created by 蜡笔小新丶 on 2017/10/9.
 */
public class SortPractice {
    public static void main(String[] args) {
        Comparable []arr = {8 ,3 ,54, 34, 12, 84, 91, 110,2329};
        System.out.println("排序前：");
        for(Comparable i:arr)
         System.out.print(i +"  ");

        Sorting.quickSort(arr,0,8);
        System.out.println("\n排序后：");
        for(Comparable i:arr)
            System.out.print(i+ "  ");
    }
}
