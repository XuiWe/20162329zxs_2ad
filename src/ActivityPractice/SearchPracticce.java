package ActivityPractice;

import chapter13.Searching;

/**
 * Created by 蜡笔小新丶 on 2017/10/9.
 */
public class SearchPracticce {
    public static void main(String[] args) {
        int []arr = {3,8, 12, 34, 54 ,84, 91, 110,2329};
        System.out.println("45二分查找："+Searching.binarySearch(arr,45));
        System.out.println("45线性查找："+Searching.linearSearch(arr,45));

        System.out.println("54线性查找：" + Searching.linearSearch(arr,54));
        System.out.println("54二分查找：" + Searching.binarySearch(arr,54));
    }
}
