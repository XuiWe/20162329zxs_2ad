package ActivityPractice;

/**
 * Created by 蜡笔小新丶 on 2017/10/9.
 */
public interface Stack<T>
{
    public void push (T element);

    public T pop() throws Exception;

    public T peek() throws Exception;

    public boolean isEmpty();

    public int size();

    public String toString();
}
