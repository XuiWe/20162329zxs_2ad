package ActivityPractice;

import junit.framework.TestCase;

/**
 * Created by 蜡笔小新丶 on 2017/10/9.
 */
public class LinkedStackTest extends TestCase {
    public void testPeek() throws Exception {
        LinkedStack<String> stack = new LinkedStack<>();
        try {
            assertEquals("Stack is empty", stack.peek());
        }catch (Exception e){
            System.out.println(e);
        }
        stack.push("zhangxusheng");
        assertEquals("zhangxusheng",stack.peek());
    }

    public void testIsEmpty() throws Exception {
        LinkedStack<String> stack = new LinkedStack<>();
        assertEquals(true,stack.isEmpty());
        stack.push("zhangxusheng");
        assertEquals(false,stack.isEmpty());
    }

    public void testSize() throws Exception {
        LinkedStack<String> stack = new LinkedStack<>();
        assertEquals(0,stack.size());
        for(int i=0;i<10;i++){
            stack.push("zhangxusheng");
        }
        assertEquals(10,stack.size());
    }

    public void testToString() throws Exception {
        LinkedStack<String> stack = new LinkedStack<>();
        assertEquals("",stack.toString());
        stack.push("zhangxusheng");
        stack.push("caiwencheng");
        stack.push("zhangtaiyu");
        stack.push("qilifeng");
        assertEquals("qilifeng"+"\n"+"zhangtaiyu"+"\n"+"caiwencheng"+"\n"+"zhangxusheng"+"\n",stack.toString());
    }

}