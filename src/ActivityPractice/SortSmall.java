package ActivityPractice;

/**
 * Created by 蜡笔小新丶 on 2017/9/23.
 */
public class SortSmall {
    int arr[];
    public SortSmall(){
        arr = new int[3];
    }

    public String Sort(int a,int b,int c){
        arr[0] = a;
        arr[1] = b;
        arr[2] = c;
        String result = "";
        for(int i=0;i<arr.length;i++){
            int min = i;
            for(int j=i+1;j<arr.length;j++){
                if(arr[i]>arr[j])
                    min = j;
            }
            swap(arr,min,i);
        }
        for(int i : arr )
            result += i + " ";
        return result;
    }

    private static void swap (int [] data, int index1, int index2)
    {
        int temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }
}
