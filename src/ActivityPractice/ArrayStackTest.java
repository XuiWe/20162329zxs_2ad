package ActivityPractice;

import junit.framework.TestCase;

/**
 * Created by 蜡笔小新丶 on 2017/10/9.
 */
public class ArrayStackTest extends TestCase {

    public void testPeek() throws Exception {
        ArrayStack<String> stack = new ArrayStack<>();
        try {
            assertEquals("Stack is empty", stack.peek());
        }catch (Exception e){
            System.out.println(e);
        }
        stack.push("zhangxusheng");
        assertEquals("zhangxusheng",stack.peek());
    }

    public void testIsEmpty() throws Exception {
        ArrayStack<String> stack = new ArrayStack<>();
        assertEquals(true,stack.isEmpty());
        stack.push("zhangxusheng");
        assertEquals(false,stack.isEmpty());
    }

    public void testSize() throws Exception {
        ArrayStack<String> stack = new ArrayStack<>();
        assertEquals(0,stack.size());
        for(int i=0;i<10;i++){
            stack.push("zhangxusheng");
        }
        assertEquals(10,stack.size());
    }

    public void testToString() throws Exception {
        ArrayStack<String> stack = new ArrayStack<>();
        assertEquals("",stack.toString());
        stack.push("zhangxusheng");
        stack.push("caiwencheng");
        stack.push("zhangtaiyu");
        stack.push("qilifeng");
        assertEquals("zhangxusheng"+"\n"+"caiwencheng"+"\n"+"zhangtaiyu"+"\n"+"qilifeng"+"\n",stack.toString());
    }

}