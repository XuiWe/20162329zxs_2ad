package cn.edu.besti.cs1623.zxs2329;

/**
 * Created by 蜡笔小新丶 on 2017/9/4.
 */

public class Contact implements Comparable {
    private String firstName, lastName, phone;

    //-------------------------------------------------------
    //  Sets up this contact with the specified information
    //-------------------------------------------------------
    public Contact(String first, String last, String telephone){
        firstName = first;
        lastName = last;
        phone = telephone;
    }

    //-------------------------------------------------------
    //  Returns a string representation of this contact.
    //-------------------------------------------------------
    @Override
    public String toString() {
        return lastName + ", " + firstName + ": " + phone;
    }

    //--------------------------------------------------------------
    // Uses both last and first names to determine lexical ordering
    //--------------------------------------------------------------
    @Override
    public int compareTo(Object o) {
        int result;

        if(lastName.equals(((Contact)o).lastName))
            result = firstName.compareTo(((Contact)o).firstName);
        else
            result = lastName.compareTo(((Contact)o).lastName);

        return result;
    }
}
