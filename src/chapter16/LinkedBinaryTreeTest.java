package chapter16;

import junit.framework.TestCase;

import java.util.ArrayList;

/**
 * Created by 蜡笔小新丶 on 2017/10/23.
 */
public class LinkedBinaryTreeTest {
    public static void main(String[] args) {

    LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        System.out.println("测试树是否为空："+ binaryTree.isEmpty());
        binaryTree.root = new BTNode<>("A");
        BTNode<String> node = binaryTree.root;

    node.right = new LinkedBinaryTree<String>("C").root;
    node.left = new LinkedBinaryTree<String>("B").root;
        (node.left).left = new LinkedBinaryTree<String>("D").root;
        (node.left).right = new LinkedBinaryTree<String>("E").root;
        (node.right).left = new LinkedBinaryTree<String>("F").root;
        (node.right).right = new LinkedBinaryTree<String>("G").root;

        try {
            System.out.println("树中是否存在C："+ binaryTree.contains("C"));
            System.out.println("树中是否存在H：" + binaryTree.contains("H"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<String> postorder = binaryTree.postorder();
        System.out.print("后序遍历：");
        for(String i : postorder){
            System.out.print(i + "\t");
        }
        ArrayList<String> preorder = binaryTree.preorder();
        System.out.print("先序遍历：");
        for(String i : preorder){
            System.out.print(i + "\t");
        }
        System.out.println("测试toString方法(中序遍历)：" + binaryTree.toString());
        System.out.println("测试树是否为空：" + binaryTree.isEmpty());
    }
}