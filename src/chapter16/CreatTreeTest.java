package chapter16;

import junit.framework.TestCase;

/**
 * Created by 蜡笔小新丶 on 2017/10/27.
 */
public class CreatTreeTest extends TestCase {
    public void testReConstructBinaryTree() throws Exception {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        System.out.println("前序输入：ABDHIEJMNCFGKL");
        String [] pr = {"A","B","D","H","I","E","J","M","N","C","F","G","K","L"};
        System.out.println("中序输入：HDIBEMJNAFCKGL");
        String [] in = {"H","D","I","B","E","M","J","N","A","F","C","K","G","L"};
        CreatTree<String> creatTree = new CreatTree<>();
        tree.root =  creatTree.reConstructBinaryTree(pr,in);
        System.out.println("唯一树的层序输出："+ tree);
    }
}