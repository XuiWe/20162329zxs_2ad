package chapter16.Practic;

import chapter16.LinkedBinaryTree;
import junit.framework.TestCase;

/**
 * Created by 蜡笔小新丶 on 2017/10/24.
 */
public class OriginalTest extends TestCase {
    public void testCreatTree() throws Exception {
        Original ori = new Original();
        LinkedBinaryTree tree = new LinkedBinaryTree();
        tree.root = ori.creatTree("2 + 3 * ( 6 + 4 ) / 2 + ( 3 + 4 )");
        System.out.println("输入中缀表达式：2 + 3 * ( 6 + 4 ) / 2 + ( 3 + 4 )");
        System.out.println("后缀表达式：" + ori.Output());
        Counts counts = new Counts();
        System.out.println("计算结果："+ counts.evaluate(ori.Output()));
    }
}