package chapter16.Practic;

import chapter16.BTNode;
import chapter16.LinkedBinaryTree;

import java.util.Scanner;

/**
 * Created by 蜡笔小新丶 on 2017/10/23.
 */
public class questionGame {
    private LinkedBinaryTree<String> tree;
    public questionGame() {
        String e1 = "是动物吗？";
        String e2 = "它有没有长尾巴？";
        String e3 = "会游泳吗？";
        String e4 = "能直立行走吗？";
        String e5 = "结果：人！";
        String e6 = "结果：昆虫！";
        String e7 = "结果：鱼！";
        String e8 = "结果：猫！";
        String e9 = "很大吗？";
        String e10 = "结果：树！";
        String e11 = "带刺吗？";
        String e12 = "结果：玫瑰！";
        String e13 = "结果：花";

        LinkedBinaryTree<String> n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13;
        n12 = new LinkedBinaryTree<>(e12);
        n13 = new LinkedBinaryTree<>(e13);
        n11 = new LinkedBinaryTree<>(e11,n12,n13);

        n10 = new LinkedBinaryTree<>(e10);
        n9 = new LinkedBinaryTree<>(e9,n10,n11);

        n8 = new LinkedBinaryTree<>(e8);
        n7 = new LinkedBinaryTree<>(e7);
        n3 = new LinkedBinaryTree<>(e3,n7,n8);

        n5 = new LinkedBinaryTree<>(e5);
        n6 = new LinkedBinaryTree<>(e6);
        n4 = new LinkedBinaryTree<>(e4,n5,n6);

        n2 = new LinkedBinaryTree<>(e2,n3,n4);

        tree = new LinkedBinaryTree<>(e1,n2,n9);
    }
    public void Game() throws Exception {
        Scanner scan = new Scanner(System.in);

        while (tree.size()>1){
            System.out.println(tree.getRootElement());
            String result = scan.nextLine();
            if(result.equalsIgnoreCase("Y"))
                tree = tree.getLeft();
            else if(result.equalsIgnoreCase("N"))
                tree = tree.getRight();
        }
        System.out.println(tree.getRootElement());
    }
}
