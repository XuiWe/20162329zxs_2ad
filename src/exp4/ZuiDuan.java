package exp4;

import java.util.Random;

/**
 * Created by 蜡笔小新丶 on 2017/11/21.
 */
public class ZuiDuan {
    public static void main(String[] args) throws Exception {
        OrthogonalList<String> SZlist1 = new OrthogonalList<>();
        String []arr1 = {"A","B","C","D","E","F"};
        for(String i:arr1)
            SZlist1.addNode(i);

        SZlist1.addSide(3, "A","B");
        SZlist1.addSide(7, "A","C");
        SZlist1.addSide(5, "B","D");
        SZlist1.addSide(5, "B","E");
        SZlist1.addSide(4, "C","E");
        SZlist1.addSide(2, "C","D");
        SZlist1.addSide(3, "E","F");
        SZlist1.addSide(6, "D","F");

        System.out.println("打印该图："+ SZlist1);

        System.out.println("A——F边最短路径：" + SZlist1.Min("A", "F"));
        System.out.println("F——A边最短路径：" + SZlist1.Min("F", "A"));
        System.out.println("B——C边最短路径：" + SZlist1.Min("B", "C"));
        System.out.println("A——D边最短路径：" + SZlist1.Min("A", "D"));
        System.out.println("C——F边最短路径：" + SZlist1.Min("C", "F"));
    }
}
