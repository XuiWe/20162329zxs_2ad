package exp4;

/**
 * Created by 蜡笔小新丶 on 2017/11/20.
 */
public class SZSide {
    int data;
    int fromVertexIndex;
    int toVertexIndex;
    SZSide nextSameFromVertex;
    SZSide nextSameToVertex;

    public SZSide(int data, int fromVertexIndex, int toVertexIndex) {
        this.data = data;
        this.fromVertexIndex = fromVertexIndex;
        this.toVertexIndex = toVertexIndex;
        nextSameFromVertex = null;
        nextSameToVertex = null;
    }

    @Override
    public String toString() {
        return "{" +
                "权值：" + data +
                ", 弧头索引：" + fromVertexIndex +
                ", 弧尾索引：" + toVertexIndex +
                ", 同弧尾：" + nextSameFromVertex +
                ", 同弧头：" + nextSameToVertex +
                '}';
    }
}
