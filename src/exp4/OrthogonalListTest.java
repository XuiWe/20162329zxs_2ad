package exp4;


import java.util.ArrayList;

/**
 * Created by 蜡笔小新丶 on 2017/11/20.
 */
public class OrthogonalListTest {
    public static void main(String[] args) throws Exception {
        OrthogonalList<String> SZlist = new OrthogonalList<>();
        String []arr = {"A","B","C","D","E"};
        for(String i:arr)
            SZlist.addNode(i);
        SZlist.addSide(0, "A","B");
        SZlist.addSide(0, "B","C");
        SZlist.addSide(0, "B","D");
        SZlist.addSide(0, "A","E");
        System.out.println("图的遍历：");
        ArrayList<String> list = SZlist.iteratorBFS(0);
        for(String i:list)
            System.out.print(i+" ");
        System.out.println("打印十字链表：");
        System.out.println("\n"+SZlist);

        SZlist.removeSide("A","B");
        System.out.println("删除边A--B");
        System.out.println("打印十字链表：");
        System.out.println(SZlist);

        System.out.println();
        System.out.println("删除元素A后遍历：");
        System.out.println("打印十字链表：");
        System.out.println(SZlist);
        SZlist.removeNode("A");
        ArrayList<String> list1 = SZlist.iteratorBFS(0);

        System.out.println("遍历结果：");
        for(String i:list1)
            System.out.print(i+" ");
    }
}