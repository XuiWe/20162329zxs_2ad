package exp4;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

/**
 * Created by 蜡笔小新丶 on 2017/11/20.
 */
public class AdjacencyMatrixTest {
    public static void main(String[] args) throws Exception {
        AdjacencyMatrix<String> adj = new AdjacencyMatrix<>();
        String [] arr = {"A","B","C","D","E"};
        for(String i:arr)
            adj.addNode(i);
        adj.addSide("A","B");
        adj.addSide("B","C");
        adj.addSide("B","D");
        adj.addSide("A","E");
        for(int i=0;i<adj.matrix.length;i++){
            for(int j=0;j<adj.matrix[i].length;j++){
                System.out.print(adj.matrix[i][j]+" ");
            }
            System.out.println();
        }
        ArrayList<String> iter = adj.iteratorBFS(0);
        for(String i:iter)
            System.out.print(i+" ");
        System.out.println();
        System.out.println("删除元素然后遍历！");
        adj.removeNode("A");
        for(int i=0;i<adj.matrix.length;i++){
            for(int j=0;j<adj.matrix[i].length;j++){
                System.out.print(adj.matrix[i][j]+" ");
            }
            System.out.println();
        }
        ArrayList<String> iter1 = adj.iteratorBFS(0);
        for(String i:iter1)
            System.out.print(i+" ");
    }
}