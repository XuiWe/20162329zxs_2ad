package exp4;

/**
 * Created by 蜡笔小新丶 on 2017/11/20.
 */
public class SZNode<E> {
    int i=0;
    E data;
    SZSide firstIn;
    SZSide firstOut;

    public SZNode(E data) {
        this.data = data;
        firstIn = null;
        firstOut = null;
    }

    @Override
    public String toString() {
        String result = " :{" +
                "结点名：" + data +
                ", 进边" + firstIn +
                ", 出边=" + firstOut +
                '}';
        return result;
    }
}
