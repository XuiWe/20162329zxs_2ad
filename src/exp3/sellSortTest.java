package exp3;

import chapter13.Sorting;

import java.util.Arrays;

/**
 * Created by 蜡笔小新丶 on 2017/11/6.
 */
public class sellSortTest {
    public static void main(String[] args) {
        int[] data = new int[] { 26, 53, 67, 48, 57, 13, 48, 32, 60, 50 };
        Sorting.shellSort(data);
        System.out.println(Arrays.toString(data));
    }
}
