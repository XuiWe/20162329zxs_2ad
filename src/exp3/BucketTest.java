package exp3;

import chapter13.Sorting;

/**
 * Created by 蜡笔小新丶 on 2017/11/6.
 */
public class BucketTest {
    public static void main(String args[]){
        int num[] = {139,999,20,60,4,80,90,400,684,0};//模拟输入
        System.out.println("排序前：");
        for(int i:num)
            System.out.print(i+" ");
        System.out.println();
        System.out.println("桶排序后：");
        Sorting.Bucket(num);
    }
}
