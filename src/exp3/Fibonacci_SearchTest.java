package exp3;

import java.util.ArrayList;

/**
 * Created by 蜡笔小新丶 on 2017/11/6.
 */
public class Fibonacci_SearchTest {
    public static void main(String[] args) {
        FibonacciList search = new FibonacciList();
        int []arr = {4,2,9,11,26,14,6,16};
        ArrayList<Integer> list = new ArrayList<>();
        for(int i:arr){
            list.add(i);
        }
        int index = search.FibonacciSearch(2,list,7);
        System.out.println("返回索引值："+ index);
        System.out.println("查找值："+ list.get(index));
    }
}