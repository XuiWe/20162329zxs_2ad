package chapter11;

/**
 * Created by 蜡笔小新丶 on 2017/8/1.
 */
public class MazeSezrch {
    public static void main(String[] args) {
        Maze labyrinth = new Maze();

        System.out.println(labyrinth);

        if(labyrinth.traverse(0,0))
            System.out.println("The maze was successfully traversed!");
        else
            System.out.println("There is no possible path.");

        System.out.println(labyrinth);
    }
}
