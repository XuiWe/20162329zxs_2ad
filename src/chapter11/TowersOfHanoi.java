package chapter11;

/*
    TowersOfHanoi.java      Java Foundations

    Represents the classic Towers of Hanoi puzzle.
 */
public class TowersOfHanoi {
    private int totalDisks;
    private int time = 0;
    /*
        用指定数量的磁盘设置谜题。
     */
    public TowersOfHanoi(int disks){
        totalDisks = disks;
    }

    /*
        对移动塔进行初始调用，以解决难题。
        使用塔2将圆盘从1号移到3号塔。
     */
    public void solve(){
        moveTower(totalDisks,1,3,2);
    }

    /*
        将指定数量的磁盘从一个塔移动到另一个塔
        通过移动n - 1个磁盘的子塔，移动一个
        磁盘，然后移动子塔回来。一个磁盘的基本情况。
     */
    private void moveTower (int numDisks, int start, int end , int temp){
        if(numDisks == 1)
            moveOneDisk(start, end);
        else{
            moveTower(numDisks-1, start, temp, end);
            moveOneDisk(start, end);
            moveTower(numDisks-1, temp, end, start);
        }
    }

    /*
        从指定的开始输出一个磁盘
        塔到指定的端塔。
     */
    private void moveOneDisk (int start, int end){
        time++;
        System.out.println("第" + time + "步： 把第 " + start + " 塔顶端的盘子移到第 " + end + " 塔");
    }
}
