package chapter11;

/*
    Porwer.java     Java Foundaations

    用递归算法计算x的y次幂
 */
public class Power {
    private int result = 1;

    public Power(int x,int y){
        count(x,y);
    }

    private void count(int x, int y){
        if(y == 1)
            result = result * x;
        else{
            result = result * x;
            count(x,y-1);
        }
    }

    public String toString(){
        return "计算结果为：" + result;
    }
}
