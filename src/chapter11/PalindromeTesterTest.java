package chapter11;

import java.util.Scanner;

/**
 * Created by 蜡笔小新丶 on 2017/8/3.
 */
public class PalindromeTesterTest {
    public static void main(String[] args) {
        String str,another = "y";

        Scanner scan = new Scanner(System.in);

        while (another.equalsIgnoreCase("y")){
            System.out.println("请输入一串字符：");
            str = scan.next();

            PalindromeTester tester = new PalindromeTester(str);
            System.out.print("如果继续请输入y（忽略大小写）");
            another = scan.next();
        }
    }
}
