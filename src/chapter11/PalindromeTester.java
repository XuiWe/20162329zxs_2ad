package chapter11;

/**
 * Created by 蜡笔小新丶 on 2017/8/3.
 */
public class PalindromeTester {
    private String str;

    public PalindromeTester(String str){
        this.str = str;
        result(0,str.length()-1);
    }

    public void result(int left, int right){
        if(left<right)
            if(str.charAt(left) == str.charAt(right))
                result(left+1,right-1);
            else
                System.out.println("这不是回文字符串！");
        else
            System.out.println("这是一个回文字符串！");
    }
}
