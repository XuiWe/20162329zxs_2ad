package chapter15.Practic;

import chapter15.javaFoundations2nd.QueueADT;

/**
 * Created by 蜡笔小新丶 on 2017/10/14.
 */
public class YHQueue {
    int fornt = 0;
    int rear = 0;
    int []queue;

    public YHQueue(){
        queue = new int[100];
        enqueue(0);
        enqueue(1);
        enqueue(0);
    }


    public void count(int n){
        for(int j = 0;j<n;j++) {
            for (int i = fornt; Math.abs(rear-i)>0; i = (i + 1) % 100) {
                System.out.print(queue[i] +"\t");
            }
            System.out.println();
            nextLine();
        }
    }

    private void nextLine() {
        int temp = rear;
        enqueue(0);
        while (Math.abs(temp-fornt)>0) {
            int A = dequeue();
            int B = first();
            enqueue(A + B);
        }
    }

    public int dequeue(){
        int result = queue[fornt];
        fornt = (fornt + 1)%100 ;
        return result;
    }

    public void enqueue(int element) {
        queue[rear] = element;
        rear = (rear + 1)%100;
    }

    public int first() {
        return  queue[fornt];
    }

    public boolean isEmpty() {
        boolean result = false;
        if (fornt == rear)
            result = true;
        return result;
    }

    public int size() {
        return rear - fornt;
    }
}
