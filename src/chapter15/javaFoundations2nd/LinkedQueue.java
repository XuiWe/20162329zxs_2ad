package chapter15.javaFoundations2nd;

/**
 * Created by 蜡笔小新丶 on 2017/10/14.
 */
public class LinkedQueue<T> implements QueueADT<T>
{
    private int count;
    private LinearNode<T> front, rear;

    public LinkedQueue()
    {
        count = 0;
        front = rear = null;
    }

    public void enqueue (T element)
    {
        LinearNode<T> node = new LinearNode<T>(element);

        if (isEmpty())
            front = node;
        else
            rear.setNext (node);

        rear = node;
        count++;
    }


    public T dequeue() throws Exception
    {
        if (isEmpty())
            throw new Exception ("queue is empty!");

        T result = front.getElement();
        front = front.getNext();
        count--;

        if (isEmpty())
            rear = null;

        return result;
    }

    public T first() throws Exception
    {
        if (isEmpty())
            throw new Exception ("queue is empty!");

        return front.getElement();
    }

    public boolean isEmpty()
    {
        return (count == 0);
    }

    public int size()
    {
        return count;
    }


    public String toString()
    {
        String result = "";
        LinearNode<T> current = front;

        while (current != null)
        {
            result = result + (current.getElement()).toString() + "\n";
            current = current.getNext();
        }

        return result;
    }
}
