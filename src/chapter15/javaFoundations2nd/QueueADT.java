package chapter15.javaFoundations2nd;

/**
 * Created by 蜡笔小新丶 on 2017/10/14.
 */
public interface QueueADT<T>
{

    public void enqueue (T element);

    public T dequeue() throws Exception;

    public T first() throws Exception;

    public boolean isEmpty();

    public int size();

    public String toString();
}
