package chapter15;

/**
 * Created by 蜡笔小新丶 on 2017/10/14.
 */
public class Customer
{
    private int arrivalTime, departureTime;

    /**
     Creates a new customer with the specified arrival time.
     arrives  the integer representation of the arrival time
     */
    public Customer (int arrives)
    {
        arrivalTime = arrives;
        departureTime = 0;
    }

    /**
     Returns the arrival time of this customer.
     the integer representation of the arrival time
     */
    public int getArrivalTime()
    {
        return arrivalTime;
    }

    /**
     Sets the departure time for this customer.
     departs  the integer representation of the departure time
     **/
    public void setDepartureTime (int departs)
    {
        departureTime = departs;
    }

    /**
     Returns the departure time of this customer.
     the integer representation of the departure time
     */
    public int getDepartureTime()
    {
        return departureTime;
    }

    /**
     Computes and returns the total time spent by this customer.
     the integer representation of the total customer time
     */
    public int totalTime()
    {
        return departureTime - arrivalTime;
    }
}
