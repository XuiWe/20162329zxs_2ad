package chapter13;

/**
 * Created by 蜡笔小新丶 on 2017/9/4.
 */
public class SearchPlayerList {
    //---------------------------------------------------------
    // Creates an array if Contact objects, then searches for a
    // particular player.
    //---------------------------------------------------------
    public static void main(String[] args) {
        Contact[] players = new Contact[7];

        players[0] = new Contact("Roder", "Federer","601-555-7384");
        players[1] = new Contact("Abdy", "Roddick","215-555-3827");
        players[2] = new Contact("Maria", "Sharapova","733-555-2969");
        players[3] = new Contact("Venus", "Williams","663-555-3984");
        players[4] = new Contact("Lleyton", "Hewitt","464-555-3489");
        players[5] = new Contact("Eleni", "Dabiilidou","322-555-2284");
        players[6] = new Contact("Serena", "Williams","243-555-2837");

        Contact target = new Contact("Roder","Federer","");

        Contact found = (Contact)Searching.linearSearch(players,target);

        if (found == null)
            System.out.println("Player was not found.");
        else
            System.out.println("Found:" + found);
    }
}
