package chapter13;

import chapter17.LinkedBinarySearchTree;
import junit.framework.TestCase;

import java.util.ArrayList;

/**
 * Created by 蜡笔小新丶 on 2017/11/6.
 */
public class SortingTest{
    public void testShellSortSmallToBig() throws Exception {
    }

    public static void main(String[] args) throws Exception {
        Comparable[] arr = {9, 5, 22, 30, 60, 13, 16, 4};
        System.out.println("排序前：");
        for(Comparable i:arr)
            System.out.print(i + " ");
        System.out.println();
        Sorting.selectionSort(arr);
        System.out.println("选择排序结果：");
        for (Comparable i : arr)
            System.out.print(i + " ");

        System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Comparable[] arr1 = {9, 5, 22, 30, 60, 13, 16, 4};
        System.out.println("排序前：");
        for(Comparable i:arr1)
            System.out.print(i+" ");
        System.out.println();
        Sorting.quickSort(arr1,0,arr.length-1);
        System.out.println("快速排序结果：");
        for (Comparable i : arr1)
            System.out.print(i + " ");


        System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Comparable[] arr2 = {9, 5, 22, 30, 60, 13, 16, 4};
        System.out.println("排序前：");
        for(Comparable i:arr2)
            System.out.print(i+" ");
        System.out.println();
        Sorting.bubbleSort(arr2);
        System.out.println("冒泡排序结果：");
        for (Comparable i : arr2)
            System.out.print(i + " ");

        System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Comparable[] arr3 = {9, 5, 22, 30, 60, 13, 16, 4};
        System.out.println("排序前：");
        for(Comparable i:arr3)
            System.out.print(i+" ");
        System.out.println();
        Sorting.insertionSort(arr3);
        System.out.println("插入排序结果：");
        for (Comparable i : arr3)
            System.out.print(i + " ");

        System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Comparable[] arr4 = {9, 5, 22, 30, 60, 13, 16, 4};
        System.out.println("排序前：");
        for(Comparable i:arr4)
            System.out.print(i+" ");
        System.out.println();
        Sorting.mergeSort(arr4,0,arr4.length-1);
        System.out.println("归并排序结果：");
        for (Comparable i : arr4)
            System.out.print(i + " ");

        System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Comparable[] arr5 = {9, 5, 22, 30, 60, 13, 16, 4};
        System.out.println("排序前：");
        for(Comparable i:arr5)
            System.out.print(i+ " ");
        System.out.println();
        LinkedBinarySearchTree tree = new LinkedBinarySearchTree();
        for(Comparable i:arr5)
            tree.add(i);
        System.out.println("(二叉树排序)正序排序：");
        ArrayList<Integer> list = tree.inorder();;
        for(int i:list)
            System.out.print(i+" ");
        System.out.println();
        System.out.println("（二叉树排序）逆序排序：");
        ArrayList<Integer> list1 = tree.PX();
        for(int i:list1)
            System.out.print(i+" ");
    }
}