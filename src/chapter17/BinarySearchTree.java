package chapter17;

//********************************************************************
//  BinarySearchTree.java       Java Foundations
//
//  Defines the interface to a binary search tree.
//********************************************************************


import chapter16.BinaryTree;

public interface BinarySearchTree<T extends Comparable<T>> extends BinaryTree<T>
{
    //  Adds the specified element to the tree.
    public void add (T element);

    //  Returns the minimum value in the binary search tree.
    public T findMin();

    //  Returns the maximum value in the binary search tree.
    public T findMax();

    //  Removes and returns the specified element from the tree.
    public T remove (T target) throws Exception;
}

