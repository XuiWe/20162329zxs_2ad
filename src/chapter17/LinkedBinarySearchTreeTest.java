package chapter17;

import chapter13.Contact;
import junit.framework.TestCase;

import java.util.ArrayList;

/**
 * Created by 蜡笔小新丶 on 2017/10/25.
 */
public class LinkedBinarySearchTreeTest {
    public static void main(String[] args) throws Exception {
        LinkedBinarySearchTree<Integer> tree = new LinkedBinarySearchTree<>();
        for(int i=50;i<100;i++)
            tree.add(2*i);
        for(int i=0;i<50;i++)
            tree.add(2*i);

        ArrayList<Integer> list = tree.levelorder();
        System.out.println("层序遍历树：");
        for(int i :list)
            System.out.print(i +" ");

        System.out.println("");

        System.out.println("查找最小值："+ tree.findMin());
        System.out.println("查找最大值："+ tree.findMax());
    }
}