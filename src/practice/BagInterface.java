package practice;

/**
 * Created by 蜡笔小新丶 on 2017/9/22.
 */

public interface BagInterface<T>{
    public boolean isEmptey();
    public boolean add(T thing);
    public void remove(T thing);
}

