package practice;

/**
 * Created by 蜡笔小新丶 on 2017/9/22.
 */
public class BagText {

    public static void main(String[] args) {
        Bag<String> bag = new Bag<>();

        System.out.println("初始袋子：");
        System.out.println(bag);
        System.out.println("初始袋子为空：");
        System.out.println(bag.isEmptey());
        System.out.println("添加一个元素打印袋子内容：");
        bag.add("Zxs");
        System.out.println(bag);
        System.out.println("袋子是否为空：");
        System.out.println(bag.isEmptey());

        System.out.println("添加再添加九个元素打印袋子内容：");
        for (int i=0;i<10;i++){
            bag.add("Cwc");
        }
        System.out.println(bag);
        System.out.println("再添加一个元素打印袋子内容：");
        bag.add("Zty");
        System.out.println(bag);

        System.out.println("删除Cwc元素打印袋子：");
        bag.remove("Cwc");
        System.out.println(bag);
    }
}
